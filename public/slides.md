---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>

# **Herramientas libres para la creación de artes visuales**

Cristian G. Guerrero

**esLibre 2024**

---
# Sobre mí...

![bg](mapa.png)

<!--
* Grado en Teleco en Granada
* Amigos en BBAA -> Comienzo a asistir a exposiciones de arte (experiencia universitaria enriquecedora)
* Nunca me había interesado el arte. De repente, concibo el arte como una forma de expresión utilizando un medio (materialidad) tan conocido que se presta al juego. Asisto a exposiciones de arte contemporáneo y fantaseo con la idea de ayudar a artistas/creadores a ser más familiares con el medio digital.
* Pero había que comer de algo: Trabajo en la industria 6 años (intento ser profesional y productivo)
* Estudio máster en profesorado, comienzo a trabajar como profe en Valencia
* Actualmente, estudio un FP en la familia Imagen y Sonido: Iluminación, captación y tratamiento de la imagen
-->


---
# Flujo de trabajo en fotografía

Etapas creativas:

1. Pre-producción
2. Producción
3. Postproducción
4. Exhibición, difusión o entrega

---
## Pre-producción

* Concepción de la idea
* Selección de modelos
* Búsqueda de localizaciones o estudios fotográficos
* Obtención de permisos
* Planificación de la iluminación
* Planificación de la toma

<!-- No todos los proyectos tienen la misma complejidad: si no hay modelos... -->
![bg left:40% 100%](https://images.unsplash.com/photo-1527011046414-4781f1f94f8c?ixlib=rb-4.0.3&q=85&fm=jpg&crop=entropy&cs=srgb)

---
## Producción

* Montaje del set
* Iluminación
* Caracterización, maquillaje y peluquería
* Toma fotográfica
* Revisión en tiempo real

<!-- Revisamos el histograma, pero también podemos ayudarnos del ordenador para hacer y visualizar la toma -->
![bg right:50% 100%](https://images.unsplash.com/photo-1550937428-659d277973ef?q=80&w=971&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)

---
## Postproducción

* Descarga y clasificación de imágenes
* Copias de seguridad
* Revelado digital
* Edición y retoque
* Montaje fotográfico
* Exportación e impresión

<!-- Aquí es donde entran en juego la mayor parte de las herramientas software -->
![bg left:40% 100%](https://images.unsplash.com/photo-1488751045188-3c55bbf9a3fa?q=80&w=987&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)

---
## Exhibición, difusión o entrega

<!-- ¡Reproducción del color! -->
![center h:450](https://images.unsplash.com/photo-1630416920377-e43f0f9dd28d?q=80&w=1166&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)


---
# Herramientas

<!--
> No digital media work can be undertaken or consumed without using tools, in the form of (often proprietary) software and hardware.
-->

> No se puede producir ni consumir obra digital alguna sin utilizar herramientas, en forma de software y hardware (a menudo propietarios).

— _Sarah Keith and Stephen Collins_
(Department of Media, Communications, Creative Arts, Language and Literature, Macquarie University, Australia)

---
## Herramientas profesionales en fotografía

<!-- Como estudiante de FP, estoy aprendiendo a usar las herramientas "profesionales" -->
![bg right:45% 100%](https://images.unsplash.com/photo-1543693087-639a38025487?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NHx8d2h5fGVufDB8MXwwfHx8Mg%3D%3D)

* MS Windows
* Paquete Adobe
* Google Drive
* Pequeñas aplicaciones en
    "la nube"

<!-- Arrrrg!!! Osea, todo mal: todo privativo... -->
<!-- ¿¿Alguien en la sala puede enseñar algo de software libre?? -->

---
# Sobre el software libre en educación

* [**Por qué las escuelas deben usar exclusivamente software libre**](https://www.gnu.org/education/edu-schools.es.html)
— _Richard Stallman_
<!--
Las escuelas tienen una misión social: enseñar a los alumnos a ser ciudadanos de una sociedad fuerte, capaz, independiente, solidaria y libre. Deben promover el uso de software libre al igual que promueven la conservación y el voto. Enseñando el software libre, las escuelas pueden formar ciudadanos preparados para vivir en una sociedad digital libre. Esto ayudará a que la sociedad entera se libere del dominio de las megacorporaciones.

Enseñar el uso de un programa que no es libre equivale, por el contrario, a inculcar la dependencia, lo cual se opone a la misión social de las escuelas. Las escuelas no deben hacerlo, nunca.

A fin de cuentas, ¿por qué algunos programadores de software privativo ofrecen a las escuelas copias gratuitas de programas que no son libres? Porque quieren utilizar a las escuelas para imponer la dependencia de sus productos, tal como las tabaqueras distribuyen cigarrillos gratuitos a los niños en edad escolar. No entregarán copias gratuitas a los estudiantes una vez que se hayan graduado, así como tampoco a las empresas para las cuales trabajarán. Una vez que uno es dependiente, se espera que pague, y las futuras actualizaciones pueden ser costosas.
-->
* [**QUIERO CANCELAR EL PLAN Y NO ME DEJAN**](https://community.adobe.com/t5/account-payment-plan-discussions/quiero-cancelar-el-plan-y-no-me-dejan/td-p/8781291?profile.language=es)
— Adobe Community
<!--
Experiencias de usuarios de Adobe que se encuentran con permanencia
-->
* [**“Must know Photoshop”: proprietary skills and media jobs in Australia**](https://journals.sagepub.com/doi/full/10.1177/1329878X221099046?casa_token=XcEdXE53onwAAAAA%3AQrUfHqsoo-MvcL7WMfzdbHDXny1tv7b7PTz20tD1eZ_7Yx4936xuIgsG7rQ4Dba-guiBt8Zm0FGl)
 — _Sarah Keith and Stephen Collins_, 2023, Sage Journals
<!--
Analiza los requisitos de software para la empleabilidad en el sector audiovisual. Discute:

The prevalence of proprietary software in recruitment advertising may also serve a more complex purpose than specifying necessary competencies.

consumers ascribe particular values to brands, employers may name specific software or platforms in order to signal values such as professionalism, creativity...

Los nombres de las herramientas propietarias también pueden usarse como sustituto de competencias digitales más amplias, ya sea por brevedad o porque el redactor del anuncio no está familiarizado con los procesos y la terminología específicos de la profesión; por ejemplo, Premiere Pro para la edición y producción de vídeo digital, Shopify para las plataformas de comercio electrónico por suscripción o PowerPoint para las competencias en presentaciones orales, visuales y digitales. Un anuncio especifica que los candidatos deben tener «experiencia con Adobe Photoshop y Adobe Premiere Pro (o herramientas de edición similares)», utilizando software propietario como abreviatura de unos conocimientos más amplios en edición digital de imágenes y vídeo. Al citar software propietario también se da valor a las habilidades digitales prácticas, distintas de las competencias teóricas o no digitales. Esto es frecuente en los criterios sobre redes sociales, que a menudo aparecen como largas listas de plataformas, por ejemplo, « Habilidad para utilizar una variedad de plataformas de redes sociales, en particular Facebook, Twitter, Instagram y YouTube». El nivel de competencia en el uso de cada una de estas plataformas, o la naturaleza del trabajo requerido, no están claros; estos criterios sirven para enfatizar la fluidez digital, la fluidez en línea y la fluidez en los medios sociales como requisito del candidato.
-->

---
### Software libre en: Pre-producción
* Nada específico todavía... (¿o sí?)

---
### Software libre en: Producción
Toma fotográfica y visualización en tiempo real

![w:353](img/open-camera.png) _ ![w:353](img/entangle.png) _ ![w:353](img/digicam.png)

---
### Software libre en: Postproducción
Descarga y clasificación de imágenes

![w:353](img/digikam.png) _ ![w:353](img/shotwell.png) _ ![w:353](img/gthumb.png)

---
### Software libre en: Postproducción
Revelado digital

![w:353](img/darktable.png) _ ![w:353](img/rawtherapee.png) _ ![w:353](img/ufraw.png)

---
### Software libre en: Postproducción
Edición, retoque, montaje fotográfico

![w:353](img/gimp.png) _ ![w:353](img/gmic.png) _ ![w:353](img/photoflare.png)

---
### Software libre en: Postproducción
Otras utilidades

![w:353](img/hugin.png) _ ![w:353](img/hdrmerge.png) _ ![w:353](img/opendronemap.png)


---
### Más software libre relacionado con la fotografía
* Lista de software, artículos, tutoriales en [**PIXLS.US**](https://pixls.us/)
* [**Inkscape**](https://inkscape.org/)
* [**Krita**](https://krita.org/)
* [**Blender**](https://www.blender.org/)
* [¡NOVEDAD!]
    https://lighting-designer.app/


---
# Discusión

<!-- Termino lanzando estas preguntas -->
<!--
* ¿Cómo podemos concienciar a los jóvenes sobre el software libre?
* ¿Por qué muchos profes no tienen conciencia sobre las licencias de software y sus implicaciones?
* ¿Hay habilidades, tal vez más difíciles de cuantificar, como el trabajo en equipo, la iniciativa, o la ética profesional, que deberíamos evaluar al nivel del conocimiento técnico?
* ¿Nos lleva el discurso de la empleabilidad a una sensación de escasez que promueve la precariedad?
* ¿Es posible crear arte cuando únicamente pensamos en producir?
-->

* ¿Qué hace que una aplicación tenga un éxito rotundo y empiece a usarse por los profesionales del sector?
* ¿Son las aplicaciones libres una alternativa a las comerciales en la industria de la fotografía?
* ¿Por qué muchos jóvenes creadores ni si quiera están dispuestos a darle una oportunidad al software libre?

---
# ¡Gracias!
Contacto:

`[mastodon]` @guyikcgg@mastodon.social
`[telegram]` @CristianCGG


<!--
Imágenes: unsplash.com, pixls.us, openstreetmap (editado con Inkscape)
-->
